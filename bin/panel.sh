#!/usr/bin/bash

MNAME=$1
FG=%{F"#ebdbb2"}
CYAN=%{F"#689d6a"}
GREEN=%{F"#b8bb26"}
YELLOW=%{F"#d79921"}
RED=%{F"#fb4934"}

Clock(){
	TIME=$(date "+%Y-%m-%d %H:%M:%S")
	echo -e -n "${TIME}" 
}

ActiveWindow(){
  echo -e -n "${CYAN}win:${FG}"
	len=$(echo -n "$(xdotool getwindowfocus getwindowname)" | wc -m)
	max_len=16
	if [ "$len" -gt "$max_len" ];then
		echo -n "$(xdotool getwindowfocus getwindowname | cut -c 1-$max_len)..."
	else
		echo -n "$(xdotool getwindowfocus getwindowname)"
	fi
}

Battery() {
  echo -e -n "${CYAN}bat:${FG}"
	PERC=$(cat /sys/class/power_supply/BAT0/capacity)
	STAT=$(cat /sys/class/power_supply/BAT0/status)
  if [[ $STAT = "Discharging" ]] ; then
    if [[ $PERC -ge 80 ]] ; then  
      PERC="${GREEN}${PERC}${FG}"
    elif [[ $PERC -ge 50 ]] ; then
      PERC="${YELLOW}${PERC}${FG}"
    elif [[ $PERC -ge 20 ]] ; then
      PERC="${RED}${PERC}${FG}"
    fi
  fi
  echo -e "${PERC}%"
}

Sound(){
  echo -e -n "${CYAN}vol:${FG}"
	NOTMUTED=$( amixer sget Master | grep "\[on\]" )
	if [[ ! -z $NOTMUTED ]] ; then
		VOL=$(awk -F"[][]" '/dB/ { print $2 }' <(amixer sget Master) | sed 's/%//g')
		echo -e "${VOL}%"
	else
		echo -e "MUTE"
	fi
}

##Language(){
##	CURRENTLANG=$(head -n 1 /tmp/uim-state)
##	if [[ $CURRENTLANG == *"English"* ]] ; then
##		echo -e " \uf0ac ENG"
##	elif [[ $CURRENTLANG == *"Katakana"* ]] ; then
##		echo -e " \uf0ac カタカナ"
##	elif [[ $CURRENTLANG == *"Hiragana"* ]] ; then
##		echo -e " \uf0ac ひらがな"
##	else
##		echo -e " \uf0ac \uf128"
##	fi
##}

while true; do
  STR="%{l}${CYAN}$MNAME:"
  STR+="%{c}$(ActiveWindow) $(Battery) $(Sound)"
	STR+="%{r}$(Clock)"
  echo -e "%{T1}$STR"
	sleep 1s
done
