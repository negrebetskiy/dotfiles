#!/bin/bash

killall lemonbar

MONS=$(xrandr | grep -o "^.* connected" | sed "s/ connected//")
X=0

for m in $(echo "$MONS"); do
  panel.sh $m | lemonbar -b -f "Hack-12" -B "#282828" -F "#ebdbb2" -g1920x20+$X+0 &
  let X=X+1920
done


